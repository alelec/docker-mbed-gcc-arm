FROM alelec/gcc-arm-none-eabi:latest
MAINTAINER https://gitlab.com/alelec/docker-mbed-gcc-arm

# Install common mbed tools and requirements
RUN pip install -U pip setuptools wheel && \
    pip install -U pyserial prettytable mbed-cli colorama PySerial \
    PrettyTable Jinja2 IntelHex junit-xml pyYAML requests mbed-ls \
    mbed-host-tests mbed-greentea beautifulsoup4 fuzzywuzzy mercurial
    
RUN pip freeze

# Install git subrepo
RUN git clone https://github.com/ingydotnet/git-subrepo /usr/share/git-subrepo;\
    cd /usr/share/git-subrepo; \
    make install

# Set mbed GCC_ARM toolchain path
RUN mbed config --global GCC_ARM_PATH "$(dirname $(which arm-none-eabi-gcc))" ;\
    mbed config --list
